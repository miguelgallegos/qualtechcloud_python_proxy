# Suds Logging
import logging
logging.basicConfig(level=logging.INFO)

# Windows NTLM Authentication for Suds
from suds.transport.https import WindowsHttpAuthenticated
ntlm = WindowsHttpAuthenticated(username='Qualtechs1\\fbdevgroup', password='gvwdooSZQg2kzL3HRpoK')

# Suds for SOAP
from suds.client import Client
from suds.xsd.doctor import Doctor, ImportDoctor, Import

# Flask API
from flask import Flask, jsonify, request
app = Flask(__name__)

def soap():
    # Follow imports on the XML file so that it can be read correctly.
    imp = Import(
        [
        'https://qualtechs1.qualtechcloud.com:4432/UtilityService/UtilityService.svc?xsd=xsd0',
        'https://qualtechs1.qualtechcloud.com:4432/UtilityService/UtilityService.svc?xsd=xsd1',
        'https://qualtechs1.qualtechcloud.com:4432/UtilityService/UtilityService.svc?xsd=xsd2',
        'https://qualtechs1.qualtechcloud.com:4432/UtilityService/UtilityService.svc?xsd=xsd3'
        ]
    )
    imp.filter.add('http://tempuri.org/')
    imp.filter.add('http://schemas.microsoft.com/2003/10/Serialization/')
    imp.filter.add('http://schemas.datacontract.org/2004/07/FB.Framework.BusinessEntity')
    imp.filter.add('http://schemas.microsoft.com/2003/10/Serialization/Arrays')
    imp.filter.add('')
    doctor = ImportDoctor(imp)

    url = 'https://qualtechs1.qualtechcloud.com:4432/UtilityService/UtilityService.svc?wsdl'

    print "Downloading & Parsing WSDL file"
    return Client(url, transport=ntlm, doctor=doctor, cache=None)

@app.route("/api/get_is_info", methods=["POST"])
def get_is_info():
    xml = soap().service.GetISInfo(
        request.form['infusion_id'],
        request.form['email_address'],
        request.form['api_key']
    )

    return jsonify(
        id = xml.Id,
        last_modified = xml.LastModified,
        modified_by = xml.ModifiedBy,
        name = xml.Name,
        uid = xml.UId,
        advisor_met_with = xml.AdvisorMetWith,
        any_concerns = xml.AnyConcerns,
        dbm = xml.DBM,
        email_address = xml.EmailAddress,
        financial_notes = xml.FinancialNotes,
        goals_motivation_for_joining = xml.GoalsMotivationForJoining,
        guest_keyword_group = xml.GuestKeywordGroup,
        is_contact_id = xml.ISContactId,
        main_speaker = xml.MainSpeaker,
        objection_type = xml.ObjectionType,
        personal_notes_story = xml.PersonalNotesStory,
        primary_keyword_group = xml.PrimaryKeywordGroup,
        rtc = xml.RTC,
        rtc_approval_date = xml.RTCApprovalDate,
        rtc_approved = xml.RTCApproved,
        rtc_request_date = xml.RTCRequestDate,
        rubric_score = xml.RubricScore
    )

if __name__ == "__main__":
    app.run(debug=False)
